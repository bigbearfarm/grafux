#-------------------------------------------------------------------------------
# Name: requests_test.py
# Purpose: yolp & ifttt webhooks work test
# referencd :
# Author   : ookuma yousuke
# Created  : 2018/08/04
# Copyright: (c) ookuma 2018
# Licence  : MIT License（X11 License）
#-------------------------------------------------------------------------------

import requests
import os,configparser
import Exec_Influxdb
import ifttt_webhooks
import YOLP_weather3

def test_ifttt(config):
    s1 = 'Sample Table Name'
    s2 = '100'
    s3 = 'this is IFTTT Webhooks TEST! Congratulations!'
    data = {"value1": s1, "value2": s2, "value3": s3 }
    url = config['ifttt']['url']

    Event_Name = url.split('/')[4]
    Your_key = url.split('/')[7]

    headers = {'Content-Type': "application/json"}
    #url = 'https://maker.ifttt.com/trigger/xxxx/with/key/xxxxxxxxxxxx'
    response = requests.post(url, json=data, headers=headers)
    print('--------< IFTTT >--------')
    if response.status_code == 200:
        print('IFTTT Webhooks is Success. Congratulations!')
    else:
        print(response.status_code)
        print('It is a failure.check your status_code.')
        print('Event_Name: ' + Event_Name)
        print('Your_key: ' + Your_key)
        print('https://maker.ifttt.com/trigger/<Event_Name>/with/key/<Your_key>')
    print('--------< IFTTT >--------')

def test_yolp(config):
    Coord ={}
    Coord = YOLP_weather3.get_coordinates(config)
    APP_ID = config['yahoo_weather']['App_ID']
#    APP_ID =''
    lon = config['locate']['longitude']
    lat = config['locate']['latitude']

    list = Coord.keys()
    COORDINATES=''
    for s in list:
        COORDINATES = COORDINATES + str(Coord[s]['lon']) + ',' + str(Coord[s]['lat']) + '%20'

    BASE_URL = "https://map.yahooapis.jp/weather/V1/place"
    OUTPUT="json"
    url = BASE_URL + "?appid=%s&coordinates=%s&output=%s&interval=5" % (APP_ID,COORDINATES,OUTPUT)
    headers = {'Content-Type': "application/json"}
    response = requests.post(url, headers=headers)

    print('--------< Y O L P >--------')
    if response.status_code == 200:
        print('YOLP is Success. Congratulations!')
    else:
        print('status_code: ' + str(response.status_code))
        print('App_ID : ' + APP_ID)
        print('longitude: ' + str(lon))
        print('latitude: ' + str(lat))
        print('YOLP is a failure.check your status_code.')

    if config['yahoo_weather']['type']=='' or config['yahoo_weather']['room'] =='' or \
         config['yahoo_weather']['region']=='' or config['yahoo_weather']['order']=='' or  config['yahoo_weather']['priority']=='':

        print('type: '+ config['yahoo_weather']['type'])
        print('room: ' + config['yahoo_weather']['room'])
        print('region: ' + config['yahoo_weather']['region'])
        print('order: ' + config['yahoo_weather']['order'])
        print('priority: ' + config['yahoo_weather']['priority'])

    print('--------< Y O L P >--------')


filepath = os.path.dirname(os.path.abspath(__file__))
filepath = os.path.dirname(filepath)  + '/grafux.cfg'
config = configparser.ConfigParser()
config.read(filepath)

test_ifttt(config)
print()
test_yolp(config)
