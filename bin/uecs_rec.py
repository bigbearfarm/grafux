#-------------------------------------------------------------------------------
# Name: uecs_rec.py
# Purpose: UECS data to influxdb
#          Ubiquitous Environment Control System
#          UECS is Japanese Green house Control System.
#          UDP BROADCAST Capture to DataBase program
# referencd : https://uecs.jp/
# Author:      ookuma yousuke
#
# Created: 2019/09/18
# Copyright:   (c) ookuma 2018
# Licence:     MIT License（X11 License）
#-------------------------------------------------------------------------------
#!/usr/bin python3
# -*- coding: utf-8 -*-
import sys,time,os
from datetime import datetime
import Initial_set,configparser
import Exec_sqlite,Exec_Influxdb

def db_write(root,config,FLG_UP,FLG_DIFF):
    data=[]
    for child in root:
      #print(child.tag)
      if child.tag == "DATA" :
        c = child.get("type")
        type = str(c[0:c.find('.')])
        table = type + "_" + child.get("room") + "_" + child.get("region") + "_" + child.get("order") + "_" + child.get("priority")
        data.append(child.text)
      if child.tag == "IP" :
        data.append(child.text)

    dt = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    table = table.lower() #small charactor
    now_data =''

    if table in FLG_DIFF :
        script = "select avg(value) from " + table  + "_diff"
        rtn = Exec_sqlite.exec(config,"select", script, table + "_diff")  # create_table_day
        if rtn is not None:
            now_data = float(data[0]) - float(rtn)
            #delete data
            script = "delete from " + table  + "_diff"
            Exec_sqlite.exec(config,"delete", script, table + "_diff")
        #insert into table_diff
        script = "insert into " + table + "_diff(time,ip_add, value) values (\'%s\',\'%s\',%f )" % (dt, data[1], float(data[0]))
        Exec_sqlite.exec(config,"insert", script, table + "_diff")

    if table in FLG_UP : #
        if table in FLG_DIFF:
            data[0] = now_data
        if config['table_name'][table] in ("on","off"):
            data[0] =str(round(float(data[0])))
            #print(table,data[0])

        #Loacl influxDB
        if config['influx_db']['local_host'] != '' :
            Exec_Influxdb.insert_influxdb(config,config['influx_db']['local_host'],table,float(data[0])*1.0,data[1],'0',None)
#リアルタイムでクラウドにデータを上げたい場合は、61行目、62行目のコメントをはずす
#Cloud influxDB 
#        if config['influx_db_cloud']['cloud_host'] != '' :
#            Exec_Influxdb.insert_influxdb(config,config['influx_db_cloud']['cloud_host'],table,float(data[0])*1.0,data[1],'0',None)


#    print(table)
    print('process id:'+ str(os.getpid()) + ' ' + table +'('+ str(data[0])+')')
#    time.sleep(0.5)
