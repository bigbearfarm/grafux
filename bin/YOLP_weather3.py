#---------------------------------------------------
# name      : YOLP_weather3.py
# Purpose   : YOLP(Yahoo weather) to database & UECS CCM send
# reference : https://qiita.com/takahirono7/items/01b1ef6bd364fffdb3c4
#             https://developer.yahoo.co.jp/webapi/map/openlocalplatform/v1/weather.html
# Author:      ookuma yousuke
#
# Created: 2019/02/07
# Copyright:   (c) ookuma 2018
# Licence:     MIT License（X11 License）
#---------------------------------------------------
#!/usr/bin/python
# encoding:utf-8

import requests,pprint,json
import Exec_Influxdb
from influxdb import InfluxDBClient
import sys,time,os,math
from datetime import datetime
import Initial_set,configparser
import geohash
from socket import timeout 

def insert_ob_influxdb(config,host_name,table,data,lat,lon,hash,dirct,type,epoch_time):
    db_user,db_pass,db_database = Exec_Influxdb.set_dbuser(config,host_name)
    client = InfluxDBClient(host_name, 8086, db_user, db_pass, db_database,timeout=3, retries=6)
    json_body = [
            {
                "measurement": table,
                "tags": {
                    "direction":dirct
                },
                "time": epoch_time,
                "precision": "s",
                "fields": {
                    "ob"  : data["ob"],
                    "geohash" :hash
                }
            }
        ]
    print("influxdb(%s): %s" %(host_name,json_body))
    try:
        client.write_points(json_body)
    except :
        pass

def insert_ccm_influxdb(config,host_name,table,ccm_data):
    db_user,db_pass,db_database = Exec_Influxdb.set_dbuser(config,host_name)
    client = InfluxDBClient(host_name, 8086, db_user, db_pass, db_database,timeout=3, retries=6)
    json_body = [
            {
                "measurement": table,
                "precision": "s",
                "fields": {
                    "value"  : float(ccm_data[0]),
                    "ccm_direction"  : str(ccm_data[1]),
                    "fc_time"  : str(ccm_data[2])
                }
            }
        ]
    print("influxdb(%s): %s" %(host_name,json_body))
    try:
        client.write_points(json_body)
    except :
        pass

def insert_fc_influxdb(config,host_name,table,data,lat,lon,hash,dirct,type):
    db_user,db_pass,db_database = Exec_Influxdb.set_dbuser(config,host_name)
    client = InfluxDBClient(host_name, 8086, db_user, db_pass, db_database,timeout=3, retries=6)
#    now = datetime.now()
#    epoch = datetime_to_epoch(now) * 1000000000'
    json_body = [
            {
                "measurement": table,
                "tags": {
                    "direction":dirct
                },
# speed up          "time": epoch,
                "precision": "s",
                "fields": {
#                    "ob"  : data["ob"],
                    "fc5" : data.get(type + "5",None),
                    "fc10": data.get(type + "10",None),
                    "fc15": data.get(type + "15",None),
                    "fc20": data.get(type + "20",None),
                    "fc25": data.get(type + "25",None),
                    "fc30": data.get(type + "30",None),
                    "fc35": data.get(type + "35",None),
                    "fc40": data.get(type + "40",None),
                    "fc45": data.get(type + "45",None),
                    "fc50": data.get(type + "50",None),
                    "fc55": data.get(type + "55",None),
                    "fc60": data.get(type + "60",None),
                    "geohash" :hash ,
                    "lat" :lat,
                    "lon" :lon
                }
            }
        ]
    print("influxdb(%s): %s" %(host_name,json_body))
    try:
        client.write_points(json_body)
    except :
        pass

def get_coordinates(config):
    lon = round(float(config['locate']['longitude']),4) #20180803
    lat = round(float(config['locate']['latitude']),4) #20180803    
    if config['yahoo_weather']['distance1'] =='':
        config['yahoo_weather']['distance1'] = '2.5' #Defalut set 2.5km
    if config['yahoo_weather']['distance2'] =='':
        config['yahoo_weather']['distance2'] = str(float(config['yahoo_weather']['distance1']) *2)   # Default set distance1 * 2

    D={} # return value dict
    Center={} #nest dict

    Center['km'] = '0'
    Center['lon'] = lon
    Center['lat'] = lat
    Center['hash'] = geohash.encode(float(lat),float(lon))
    D['Center'] = Center
    for distance in [config['yahoo_weather']['distance1'],config['yahoo_weather']['distance2']]:
        for s in ['N','E','S','W']:
            N={} # nest dict
            E={} # nest dict
            S={} # nest dict
            W={} # nest dict
            coord = round(float(distance)*0.01,4)
            if s== 'N': # North
                N['km'] =distance
                N['lon'] = str(round(float(lon) + coord ,4))
                N['lat'] = lat
                N['hash'] = geohash.encode(float(N['lat']),float(N['lon']))
                D[s + distance] = N
            elif s == 'E': # East
                E['km'] = distance
                E['lon'] = lon
                E['lat'] = str(round(float(lat) + coord ,4))
                E['hash'] = geohash.encode(float(E['lat']),float(E['lon']))
                D[s + distance] = E
            elif s == 'S': # South
                S['km'] = distance
                S['lon'] = str(round(float(lon) - coord ,4))
                S['lat'] = lat
                S['hash'] = geohash.encode(float(S['lat']),float(S['lon']))
                D[s + distance] = S
            elif s == 'W': # West
                W['km'] = distance
                W['lon'] = lon
                W['lat'] = str(round(float(lat) - coord ,4))
                W['hash'] = geohash.encode(float(W['lat']),float(W['lon']))
                D[s + distance] = W
            del N,E,S,W
    return D

def get_yahoo_weather(config,ip_add,Coord):
    APP_ID = config['yahoo_weather']['App_ID']
    lon = config['locate']['longitude']
    lat = config['locate']['latitude']

    list = Coord.keys()
    COORDINATES=''
    for s in list:
        COORDINATES = COORDINATES + str(Coord[s]['lon']) + ',' + str(Coord[s]['lat']) + '%20'
#    print(COORDINATES)

    BASE_URL = "https://map.yahooapis.jp/weather/V1/place"
    OUTPUT="json"
    url = BASE_URL + "?appid=%s&coordinates=%s&output=%s&interval=5" % (APP_ID,COORDINATES,OUTPUT)
    print (url)

#    try:
#        response = urllib.request.urlopen(url, timeout=15).read()
#    except timeout:
#        return None

    try:
        response = requests.get(url)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # 20221007 例外処理追加
        print("YahooAPI エラー : ",e)
        sys.exit()
#    else:
#        print("正常に処理されました")
    finally:
        pass
        
#    j = json.loads(response.decode("utf-8"))
    j = json.loads(response.text)

    CCM=[]
    ccm_data=[0,'','']
    for k in range(len(j['Feature'])):
        geo_lon = j['Feature'][k]['Geometry']['Coordinates'].split(",")[0]
        geo_lat = j['Feature'][k]['Geometry']['Coordinates'].split(",")[1]
        geo_hash = geohash.encode(round(float(geo_lat),4),round(float(geo_lon),4))
        for s in list:
            if str(Coord[s]['hash']) == str(geo_hash):
                Dirct = s # Coord.keys()
                km = float(Coord[s]['km'])

        date = []
        rainfall=[]
        type =[]
        data = {}
        for i in range(len(j['Feature'][k]['Property']['WeatherList']['Weather'])):
            type.append(j['Feature'][k]['Property']['WeatherList']['Weather'][i]['Type'])
            now = datetime.now()
            forcast_time = datetime.strptime(j['Feature'][k]['Property']['WeatherList']['Weather'][i]['Date'], '%Y%m%d%H%M')
            delta = forcast_time - now
            fc_time = math.floor(math.floor(delta.total_seconds()/60+5)/5)*5
#            print(now,forcast_time,str(fc_time),j['Feature'][k]['Property']['WeatherList']['Weather'][i]['Rainfall'])
            if type[i]=='observation':
                epoch_time = int(time.mktime(forcast_time.timetuple())) * 1000000000
                data['ob'] = float(j['Feature'][k]['Property']['WeatherList']['Weather'][i]['Rainfall'])*1.0
            if fc_time >= 5 : #forcast time check
                data[config['yahoo_weather']['type'] + str(fc_time)] = float(j['Feature'][k]['Property']['WeatherList']['Weather'][i]['Rainfall'])*1.0

#       CCM data
        if config['yahoo_weather']['send_ccm_time']=='':
            config['yahoo_weather']['send_ccm_time']='20' # Defalut set 20 Minute
        if config['yahoo_weather']['send_ccm_range']=='':
            config['yahoo_weather']['send_ccm_range']='0' # Defalut set 0km = Center
        m = int(config['yahoo_weather']['send_ccm_time'])//5 # check time
        for mm in range(1,m+1):
            if float(config['yahoo_weather']['send_ccm_range'])*1.0 >= km: #check range km
                if ccm_data[0] < data[config['yahoo_weather']['type'] + str(mm*5)]: # check MAX data
                    ccm_data[0] = data[config['yahoo_weather']['type'] + str(mm*5)]
                    ccm_data[1] = str(Dirct)
                    ccm_data[2] = 'fc' + str(mm*5)
                    print('Coordnates: ' + ccm_data[1] + ' forcast:' + str(ccm_data[2]) + '  CCM :' + str(ccm_data[0]))

#       local Forcast & Observation
        insert_fc_influxdb(config,config['influx_db']['local_host'],'YOLP',data,geo_lat,geo_lon,geo_hash,Dirct,config['yahoo_weather']['type'])
        insert_ob_influxdb(config,config['influx_db']['local_host'],'YOLP_ob',data,geo_lat,geo_lon,geo_hash,Dirct,config['yahoo_weather']['type'],epoch_time)
#        cloud Forcast & Observation
        if config['influx_db_cloud']['cloud_host']!='':
            insert_fc_influxdb(config,config['influx_db_cloud']['cloud_host'],'YOLP',data,geo_lat,geo_lon,geo_hash,Dirct,config['yahoo_weather']['type'])
            insert_ob_influxdb(config,config['influx_db_cloud']['cloud_host'],'YOLP_ob',data,geo_lat,geo_lon,geo_hash,Dirct,config['yahoo_weather']['type'],epoch_time)

        del date,rainfall,type,data
#   CCM SEND
    type = config['yahoo_weather']['type'] + '.oMC'
    msg = "<?xml version=\"1.0\"?><UECS ver=\"1.00-E10\">"
    msg = msg + "<DATA type=\"" +  type + "\""
    msg = msg + " room=" + "\"" +  config['yahoo_weather']['room'] + "\""
    msg = msg + " region=" + "\"" +  config['yahoo_weather']['region'] + "\""
    msg = msg + " order=" + "\"" +  config['yahoo_weather']['order'] + "\""
    msg = msg + " priority=" + "\"" +  config['yahoo_weather']['priority'] + "\">"
    msg = msg + str(ccm_data[0]) + "</DATA>"
    msg = msg + "<IP>" + ip_add + "</IP></UECS>"
    CCM.append(msg)

#    print(msg)
    insert_ccm_influxdb(config,config['influx_db']['local_host'],'CCM',ccm_data)
    if config['influx_db_cloud']['cloud_host']!='':
        insert_ccm_influxdb(config,config['influx_db_cloud']['cloud_host'],'CCM',ccm_data)
    return CCM

"""
#filepath = os.path.dirname(os.getcwd()) + '/uecs_c.cfg'
filepath = os.path.dirname(os.path.abspath(__file__))
filepath = os.path.dirname(filepath)  + '/grafux.cfg'
config = configparser.ConfigParser()
config.read(filepath)

Coord ={}
Coord = get_coordinates(config)
print(Coord)
get_yahoo_weather(config,'192.168.1.13',Coord)
#print("test")
"""