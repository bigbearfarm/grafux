#-------------------------------------------
# UTC Sunrise Sunset Calculate 
# reference: http://rhodesmill.org/pyephem/rise-set.html
# install : sudo pip3 install pyephem
# referencd : http://rhodesmill.org/pyephem/rise-set.html
# Author:     ookuma yousuke
#
# Created: 2018/07/30
# Copyright:   (c) ookuma 2018
# Licence:     MIT License（X11 License）
#-------------------------------------------
#!/usr/bin/env python
import ephem,time
#import datetime, time
from datetime import datetime, timedelta, timezone

def date2epoch(daytime):
    dt = str(daytime).split('-')
    dt_day = dt[2].split(' ')
    dt_time = dt_day[1].split(':')

    year= int(dt[0])
    month = int(dt[1])
    day = int(dt_day[0])
    h= int(dt_time[0])
    m= int(dt_time[1])
    t = datetime(year,month,day,h,m)

    return str(time.mktime(t.timetuple())).split('.')[0] + '000000000'

def sun_exec(basedate,lon,lat):
    #ephem
    pl = ephem.Observer()
    pl.lat, pl.lon , pl.elevation = lat,lon ,0
#    pl.date = basedate

#    now=datetime.now()
#    t=datetime(now.year,now.month,now.day,12,00)
    t=datetime(basedate.year,basedate.month,basedate.day,basedate.hour,basedate.minute)
    unix_time=int(time.mktime(t.timetuple()))
    pl.date = datetime.utcfromtimestamp(unix_time)

    pre_sunrise_utc = pl.previous_rising(ephem.Sun())
    pre_sunset_utc = pl.previous_setting(ephem.Sun())
    nex_sunrise_utc = pl.next_rising(ephem.Sun())
    nex_sunset_utc = pl.next_setting(ephem.Sun())

    pre_sunset_jst = ephem.localtime(pre_sunset_utc)
    pre_sunrise_jst = ephem.localtime(pre_sunrise_utc)
    nex_sunrise_jst = ephem.localtime(nex_sunrise_utc)
    nex_sunset_jst = ephem.localtime(nex_sunset_utc)

    JST = timezone(timedelta(hours=+9), 'JST')
    UTC = timezone(timedelta(hours=-9), 'UTC')
    now_jst = datetime.now(JST).strftime('%s')+'000000000'

    pre_sr = date2epoch(pre_sunrise_jst)
    pre_ss = date2epoch(pre_sunset_jst)
    nex_sr = date2epoch(nex_sunrise_jst)
    nex_ss = date2epoch(nex_sunset_jst)

    H12s = str(basedate.year) +'/'+ str(basedate.month) +'/'+ str(basedate.day) + ' 12:00:00' #12h
    H12dt = datetime.strptime(H12s, '%Y/%m/%d %H:%M:%S') # str2datetime
    H12 = H12dt.strftime('%s') + '000000000'

    H00s= str(basedate.year) +'/'+ str(basedate.month) +'/'+ str(basedate.day) + ' 00:00:00' #0h
    H00dt = datetime.strptime(H00s, '%Y/%m/%d %H:%M:%S') # str2datetime
    H00 = H00dt.strftime('%s') + '000000000'

    H24s= str(basedate.year) +'/'+ str(basedate.month) +'/'+ str(basedate.day) + ' 23:59:59' #24h
    H24dt = datetime.strptime(H24s, '%Y/%m/%d %H:%M:%S') # str2datetime
    H24 = H24dt.strftime('%s') + '000000000'

#    H12_utc = H12dt - timedelta(hours=9)  12:00
    H12_utc = H12dt - timedelta(hours=12)   #09:00

    return now_jst,pre_sr,pre_ss,nex_sr,nex_ss,H12,H12_utc,H00,H24

""" 
    pre_sunset_jst = ephem.localtime(pre_sunset_utc)
    pre_sunrise_jst = ephem.localtime(pre_sunrise_utc)
    nex_sunrise_jst = ephem.localtime(nex_sunrise_utc)
    nex_sunset_jst = ephem.localtime(nex_sunset_utc)

    now = datetime.now()
    now_utc = now.strftime('%s')+'000000000'
    pre_sr =  pre_sunrise_jst.strftime('%s')+'000000000'
    pre_ss =  pre_sunset_jst.strftime('%s')+'000000000'

    H12s  = datetime.now().strftime('%Y/%m/%d 12:00:00')
    H12dt = datetime.strptime(H12s,'%Y/%m/%d %H:%M:%S')
    H12 = H12dt.strftime('%s') + '000000000'

    nex_sr = nex_sunrise_jst.strftime('%s')+'000000000'
    nex_ss = nex_sunset_jst.strftime('%s')+'000000000'

    return now_utc,pre_sr,pre_ss,H12,nex_sr,nex_ss
"""

