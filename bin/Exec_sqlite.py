#!/usr/bin python3
# -*- coding: utf-8 -*-
# parameter
#
# cmd        : not null : "None,create,insert,update,"
# script     : not null : "None, select * from table" or "update_table.sql"
# root_table : null     :table
#Initialset_create table
import sqlite3,os


def exec(config,cmd ,script,table):
    #db_file = os.path.dirname(os.getcwd()) + '/data/db.sqlite3'
    filepath = os.path.dirname(os.path.abspath(__file__))
    db_file = os.path.dirname(filepath)  + '/data/db.sqlite3'

    if not os.path.exists(db_file):
        open(db_file, 'w')

    conn = sqlite3.connect(db_file)
    c = conn.cursor()

    if cmd in ('delete','insert','create')  : # return is None
        c.execute(script)
        rtn = 0
    else  : # select
        c.execute(script)
        rtn = c.fetchone()[0]

    conn.commit()
    conn.close()

    return rtn

# cnt!= 0 data ari
#script="create table if not exists wairtemp_1_2_1_1 (time datetime not null,ip_add varchar(20) not null, value varchar(20) not null);"
#exec("create",script,"wairtemp_1_2_1_1",None)
#exec("create","sqlite_create_table.sql","wairtemp_1_2_1_1" , "wairtemp_1_2_1_1_day") #create_table_day

#print("test")

