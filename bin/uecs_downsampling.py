#-------------------------------------------------------------------------------
# Name: uecs_downsampling.py
# Purpose: Influxdb Downsampling and Method for management of empty capacity 
# referencd : https://uecs.jp/
# Author:      ookuma yousuke
#
# Created: 2019/02/09
# Copyright:   (c) ookuma 2018
# Licence:     MIT License（X11 License）
#-------------------------------------------------------------------------------
#!/usr/bin python3
# -*- coding: utf-8 -*-

import sys,time,os,gc
from datetime import datetime, timedelta, timezone,date
import Initial_set,configparser
import Exec_Influxdb
import pandas as pd
from os.path import join, getsize
from influxdb import InfluxDBClient,DataFrameClient

def exec_downsamling(config,host_name,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC):
    for table in FLG_UP:
        func = ' mean(value) '
        if table in FLG_MAX :
            func = ' max(value) '
        elif table in FLG_DIFF :
            func = ' sum(value) '

#        print('DownSampling / '+ host_name + ' : ' + table )
        r = Exec_Influxdb.select_influxdb(config,host_name,table,func,None)
        if r is not None:
            data = list(r.get_points(measurement=table))
            df = pd.DataFrame(data)
            df = df.dropna(axis = 0, how = 'any')

            if len(df.index) > 1:
                print('DownSampling / '+ host_name + ' : ' + table + ' / Count: ' +str(len(df.index)))
#                print(list(df['time']))
#                print(list(df['value']))
                df['time'] = pd.to_datetime(df['time'])
                df['value'] = list(map(lambda x : x * 1.0 ,df['value']))
                dataframe = pd.DataFrame(data={'value':list(df['value'])} ,index=list(df['time']))

                # DataFrameClient
                db_user,db_pass,db_database = Exec_Influxdb.set_dbuser(config,host_name)
                client = DataFrameClient(host_name, 8086, db_user, db_pass, db_database, timeout=3, retries=6)
#                time.sleep(1)
                try:
                    client.write_points(dataframe, table, {'sum': '1'} ,protocol='line') #20220920 修正
                except:
                    pass
                script = 'DELETE FROM \"%s\" where time < now()-24h and sum = \'0\';  ' % (table)
#                time.sleep(1)
                try:
                    client.query(script)
                except:
                    pass

                del df,dataframe
                # メモリ解放
                gc.collect()

    if config['yahoo_weather']['App_ID']!='':
        script= 'DELETE FROM \"YOLP\" WHERE time < now()-30d;'
        script= script + ' DELETE FROM \"YOLP_ob\" WHERE time < now()-30d;'
        script= script + ' DELETE FROM \"CCM\" WHERE time < now()-30d;'
        print('DELETE yahoo_weather 30day ago;')
        time.sleep(1)
        Exec_Influxdb.delete_influxdb(config,host_name,None,script)


def chk_disk(config):
    path = '/'
    st = os.statvfs(path)

    # G byte
    total = round(st.f_frsize * st.f_blocks/1000000000.0 ,2)
    used = round(st.f_frsize * (st.f_blocks - st.f_bfree)/1000000000.0 ,2)
    free = round(st.f_frsize * st.f_bavail/1000000000.0 ,2)
    print('total: ', total)
    print(' used: ', used)
    print(' free: ', free)

    used = round((used / total * 100.0),2)

    script=( 'delete from \"disk_capa\";')
    Exec_Influxdb.delete_influxdb(config,config['influx_db']['local_host'],'disk_capa',script)
    Exec_Influxdb.insert_influxdb(config,config['influx_db']['local_host'],'disk_capa',used,None,'1',None)
    if config['influx_db_cloud']['cloud_host'] != '':
        Exec_Influxdb.delete_influxdb(config,config['influx_db_cloud']['cloud_host'],'disk_capa',script)
        Exec_Influxdb.insert_influxdb(config,config['influx_db_cloud']['cloud_host'],'disk_capa',used,None,'1',None)

    print(str(used)+'%')
    if used > 90 :
        print("no disk")
        for table in FLG_UP:
            r = {}
            script = 'select * from \"%s\" limit 2;' % (table)
            r = Exec_Influxdb.select_influxdb(config,config['influx_db']['local_host'],table,None,None,script)
            for x in r:
                for i in x:
                    old_time = x[1]['time']
                    break
                break

            print(str(old_time))
            del_day= str(24*20)+'h' #delete 20days
            script = 'delete  from \"%s\" where time < (\'%s\' - %s);' % (table,str(old_time),del_day)
            Exec_Influxdb.delete_influxdb(config,config['influx_db']['local_host'],table,script)

    else :
        print("ok disk")


def downsamling(config,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC):
    exec_downsamling(config,config['influx_db']['local_host'],FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC)
#    if config['influx_db_cloud']['cloud_host'] != '':
#        exec_downsamling(config,config['influx_db_cloud']['cloud_host'],FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC)
    chk_disk(config)


"""
# read config
filepath = os.path.dirname(os.path.abspath(__file__))
filepath = os.path.dirname(filepath)  + '/grafux.cfg'
config = configparser.ConfigParser()
config.read(filepath)

# FLG_Cloud up
FLG_UP=[]
FLG_DIFF=[]
FLG_MAX=[]
FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC = Initial_set.chk_table(config)

downsamling(config,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC)
"""
