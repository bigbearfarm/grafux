#-------------------------------------------------------------------------------
# Name: Nuro1km.py
# Purpose: Ministry of Agriculture, Forestry and Fisheries
# referencd : https://amu.rd.naro.go.jp/wiki_open/doku.php
# Author:      ookuma yousuke
#
# Created: 2019/12/30
# Copyright:   (c) ookuma 2018
# Licence:     MIT License（X11 License）
#-------------------------------------------------------------------------------
#!/usr/bin python3
# -*- coding: utf-8 -*-

try:
    import AMD_Tools3 as AMD
except ImportError:
    pass
import os,configparser
from influxdb import InfluxDBClient,DataFrameClient
import time,geohash
from datetime import datetime,timedelta,timezone
import Exec_Influxdb,ifttt_webhooks
import numpy as np
import pandas as pd

def get_last_time(config,host,table):
    script = 'SHOW MEASUREMENTS;'
    r = Exec_Influxdb.select_influxdb(config,host,table,None,script)
    if r is None:
        return None

    table_list=[]
    for x in list(r):
        for i in range(0,len(x)):
            table_list.append(x[i]['name'])

    if table not in table_list:
        return None

    if table in table_list:
        script='select * from \"' + table + '\" order by time desc limit 1;'
        r = Exec_Influxdb.select_influxdb(config,host,table,None,script)
        if r is not None:
            for x in r:
#            print(table , x[0]['time'])
                script = 'delete from \"%s\" where time > \'%s\' ;' % (table,x[0]['time'])
                Exec_Influxdb.delete_influxdb(config,host,table,script)
                return x[0]['time']
        else:
            return 'skip'

def insert_influxdb(config,host_name,table,df,data):
    db_user,db_pass,db_database = Exec_Influxdb.set_dbuser(config,host_name)
    client = DataFrameClient(host_name, 8086, db_user, db_pass, db_database, timeout=3, retries=6 )

    tags = { 'tag_mesh': data['mesh'], 'tag_hash': data['hash'] }
    try:
        client.write_points(df,table,tags, protocol='json')
    except :
        pass

def chk_cmt(d):
    now = datetime.now()
    if now > d :
        return 1 # pass day
    return 0

def get_mesh(config,host_name):
    lon = round(float(config['locate']['longitude']),2)
    lat = round(float(config['locate']['latitude']),2)
    now = datetime.now()

    #element_list = ['TMP_mea','TMP_max','TMP_min','APCP','SSD','GSR','DLR','RH','WIND','SD','SWE','SFW'] #,'PTMP']
    element_list = ['TMP_mea','TMP_max','TMP_min']
    start_day="2008-01-01"

    #timedomain = [ "2019-11-01", "2019-11-30" ] #期間の設定。
    #lalodomain = [ lat1, lat2, lon1, lon2]
    lalodomain = [ lat, lat, lon, lon]
    start = time.time()

    ift_log=""
    for element in element_list:
        table_name = 'Mesh_' + element

        script = 'delete from \"%s\" where time > now() -7d ; ' % (table_name)
#        script = 'delete from \"%s\" ' % (table_name)
        Exec_Influxdb.delete_influxdb(config,host_name,table_name,script)

        #date start
        r_day = get_last_time(config,host_name,table_name)
        if r_day=='skip':
            continue
        if r_day is not None:
            start_day = r_day.split('-')[0] +'-'+ r_day.split('-')[1] +'-'+ r_day.split('-')[2].split('T')[0]

        # date end
        end_day = now - timedelta(days=1)
        if element in ('TMP_mea','TMP_max','TMP_min','APCP','PTMP'):
            end_day =  now + timedelta(days=26)
        elif  element in ('RH','WIND'):
            end_day =  now + timedelta(days=9)
        end_day = str(end_day.year) +'-'+ str(end_day.month) +'-'+ str(end_day.day)
       # set date
        timedomain = [ start_day, end_day]
        print(timedomain)

        # 気象データの取得
        time.sleep(3)
        try:
            Msh,tim,lat,lon = AMD.GetMetData(element,timedomain,lalodomain)
        except :
            ift_log += " <br> element : " + str(element)
            continue

        cnt = 0
        s_cnt = Msh.shape[0] * Msh.shape[1] * Msh.shape[2]

        #df1 Numpy.dfarry to pandas
        data={}
        data["mesh"] = AMD.lalo2mesh(lat[0],lon[0])
        data["hash"] = geohash.encode(float(lat[0]),float(lon[0]))
        data["cmt"] = list(map(chk_cmt,tim[0:len(tim)]))

        df1 = pd.DataFrame(data=
                        {"value":Msh[0:len(tim),0,0]
                        ,"lat":lat[0]
                        ,"lon":lon[0]
                        ,"mesh":AMD.lalo2mesh(lat[0],lon[0])
                        ,"hash":geohash.encode(float(lat[0]),float(lon[0]))
                        ,"cmt":data["cmt"]
                       }
                       ,index=tim[0:len(tim)]
                       )



        insert_influxdb(config,host_name,table_name,df1,data)
#    print(df1)
        elapsed_time = time.time() - start
        print ("elapsed_time:{0}".format(elapsed_time) + "[sec]")
        time.sleep(1)

    if ift_log != "" and config['ifttt']['url'] != "":
        url = config['ifttt']['url']
        ifttt_webhooks.Exec_ifttt(url,ift_log,"メッシュ農業気象データシステム データ取得エラー","")
        print("Error ifttt! ")

def get_nuro1km(config):
    get_mesh(config,config['influx_db']['local_host'])
    if config['influx_db_cloud']['cloud_host']!='':
        get_mesh(config,config['influx_db_cloud']['cloud_host'])


""" 
# read config
filepath = os.path.dirname(os.path.abspath(__file__))
filepath_AMD = os.path.dirname(filepath)  + '/bin/AMD_Tools3.py'
filepath = os.path.dirname(filepath)  + '/grafux.cfg'
config = configparser.ConfigParser()
config.read(filepath)
print(filepath_AMD)
if os.path.exists(filepath_AMD):
    get_nuro1km(config)
""" 
