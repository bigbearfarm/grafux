#-------------------------------------------------------------------------------
# Name: Initial_set.py
# Purpose: Ubiquitous Environment Control System
#          UECS is Japanese Green house Control System.
#          UDP BROADCAST Capture to DataBase program
# referencd : https://uecs.jp/
# Author:      ookuma yousuke
#
# Created: 2018/07/30
# Copyright:   (c) ookuma 2018
# Licence:     MIT License（X11 License）
#-------------------------------------------------------------------------------
import os
import configparser
import Exec_sqlite

# -*- coding: utf-8 -*-

def chk_table(config):
    #read cfg
#    filepath = os.path.dirname(os.getcwd()) + '/grafux.cfg'
#    filepath = os.path.dirname(os.path.abspath(__file__)) 
#    filepath = os.path.dirname(filepath)  + '/grafux.cfg'
#    config = configparser.ConfigParser()
#    config.read(filepath)

    FLG_UP=[]
    FLG_DIFF=[]
    FLG_MAX=[]
    FLG_ABC=[]
    for tb in config.options('table_name'):
        # create data store table
        if config['table_name'][tb] !="":
            FLG_UP.append(tb)

        # last data table
        if config['table_name'][tb] == "diff":
            FLG_DIFF.append(tb)
            tb_diff = tb + "_diff"
            script  = "create table if not exists " + tb_diff + "(time datetime not null,ip_add varchar(20) not null, value float not null);"
            print(tb_diff +' : ' + script)
            Exec_sqlite.exec(config,'create',script,tb_diff) #create_table_sum
       
        # aggregate 
        if config['table_name'][tb] in ("on","off"):
            FLG_MAX.append(tb)
        if config['table_name'][tb] == "abc":
            FLG_ABC.append(tb)

    return FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC

""" 
def chk_Aggregate():
    #read cfg
#    filepath = os.path.dirname(os.getcwd()) + '/uecs_c.cfg'
    filepath = os.path.dirname(os.path.abspath(__file__))
    filepath = os.path.dirname(filepath)  + '/uecs_c.cfg'
    config = configparser.ConfigParser()
    config.read(filepath)

    FLG_ABC=[]
    FLG_SUM=[]
    FLG_MAX=[]
    for tb in config.options('table_name'):
        # create data store table
        if config['table_name'][tb] =="abc":
            FLG_ABC.append(tb)
        if config['table_name'][tb] =="diff":
            FLG_SUM.append(tb)
        if config['table_name'][tb] in ("on","off"):
            FLG_MAX.append(tb)

    return FLG_ABC,FLG_SUM,FLG_MAX

""" 
