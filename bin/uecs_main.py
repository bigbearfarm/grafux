#-------------------------------------------------------------------------------
# Name: uecs_main.py
# Purpose: UECS data to influxdb
#          Ubiquitous Environment Control System
#          UECS is Japanese Green house Control System.
#          UDP BROADCAST Capture & Send program
# referencd : https://uecs.jp/
# Author:      ookuma yousuke
#
# Created: 2019/09/18
# Copyright:   (c) ookuma 2019
# Licence:     MIT License(X11 License)
#-------------------------------------------------------------------------------
#!/usr/bin python3
# -*- coding: utf-8 -*-

from socket import *
import sys,time,os ,uecs_rec
import xml.etree.ElementTree as ET
from datetime import datetime
import Initial_set,configparser
import chk_process
from multiprocessing import Process
import threading,queue
import YOLP_weather3 as YOLP
import ifttt_webhooks as IFT
import uecs_downsampling as DS
import uecs_aggregate2 as AG
import Exec_Influxdb
import Nuro1km as NURO
import schedule,functools
import logging


# Global Parmeter
# read config
filepath = os.path.dirname(os.path.abspath(__file__))
logfile = os.path.dirname(filepath)  + '/data/error.log' #LOG
filepath = os.path.dirname(filepath)  + '/grafux.cfg' #CNF
config = configparser.ConfigParser()
config.read(filepath)

# LOG
# LOG Level ->  DEBUG,INFO,WARNING,ERROR,CRITICAL
logging.basicConfig(filename= logfile,level=logging.WARNING)

# FLG up
FLG_UP=[]
FLG_DIFF=[]
FLG_MAX=[]
FLG_ABC=[]
FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC = Initial_set.chk_table(config)

#check InfluxDB
if config['influx_db_cloud']['cloud_host'] != '':
    if chk_process.chkInfluxDB(config['influx_db_cloud']['cloud_host']) is False:
        config['influx_db_cloud']['cloud_host']=''

# GeoCode
Coord ={}
Coord = YOLP.get_coordinates(config)

# get ip address
ip = socket(AF_INET, SOCK_DGRAM)
ip.connect(("8.8.8.8", 80))
ip_add = ip.getsockname()[0]
ip.close()

HOST = ''
PORT = 16520
ADDRESS = "255.255.255.255"

s=socket(AF_INET,SOCK_DGRAM)
s.bind((HOST, PORT))
s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

def isNone(i):
    if i is None:
        return 0
    else:
        return i

def ccm_rec():
    msg, address = s.recvfrom(8192)
    XmlData = msg.decode('utf-8')
    root = ET.fromstring(XmlData)
    p = Process(target=uecs_rec.db_write, args=(root,config,FLG_UP,FLG_DIFF))
    p.start()
    if p.join(5) is None:
        p.terminate()

def job_cloud_up():
    if config['influx_db_cloud']['interval']!='':
        print('START Cloud up')
        p = Process(target=Exec_Influxdb.cloud_data_up, args=(config,FLG_UP))
        p.start()
        if p.join(30) is None:
            if isNone(p.exitcode) <= 0 :
                p.terminate()
                p.join()

CCM=[]
def job_yolp():
    if config['yahoo_weather']['App_ID']!='':
        print('START YOLP')
        global CCM
        CCM = YOLP.get_yahoo_weather(config,ip_add,Coord)
        p = Process(target=job_yolp_ccm())
        p.start()
        if p.join(10) is None:
            if isNone(p.exitcode) <= 0 :
                p.terminate()
                p.join()
        print('END YOLP')

def job_yolp_ccm():
    global CCM
    if len(CCM)>0:
        print('START YOLP_CCM')
        for msg in CCM :
#            s.sendto(msg.encode('utf-8'), (ADDRESS, PORT))
            p = Process(target=s.sendto, args=(msg.encode('utf-8'), (ADDRESS, PORT)))
            p.start()
            if p.join(10) is None:
                if isNone(p.exitcode) <= 0 :
                    p.terminate()
                    p.join()
            print(msg)
            print('END YOLP_CCM')

def job_ifttt():
    if config['ifttt']['url'] !='':
        print('START IFTTT')
        p = Process(target=IFT.chk_ifttt, args=(config,)) # args as tuple
        p.start()
        if p.join(20) is None:
            if isNone(p.exitcode) <= 0 :
                p.terminate()
                p.join()
        print('END IFTTT')

def job_ds():
    print('START DOWNSAMPLING')
    p = Process(target=DS.downsamling, args=(config,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC))
    p.start()
    if p.join(600) is None:
        if isNone(p.exitcode) <= 0 :
            p.terminate()
            p.join()
    print('END DOWNSAMPLING')

def job_ag():
    print('START AGGRIGATE')
    p = Process(target=AG.aggregate, args=(config,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC))
    p.start()
    if p.join(600) is None:
        if isNone(p.exitcode) <= 0 :
            p.terminate()
            p.join()
    p = Process(target=AG.time_ag, args=(config,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC))
    p.start()
    if p.join(600) is None:
        if isNone(p.exitcode) <= 0 :
            p.terminate()
            p.join()
    print('END AGGRIGATE')

def job_nuro():
    filepath1_AMD = os.path.dirname(filepath)  + '/bin/AMD_Tools3.py'
    if os.path.exists(filepath1_AMD):
        print('START NURO 1km Mesh')
        p = Process(target=NURO.get_nuro1km, args=(config,))
        p.start()
        if p.join(600) is None:
            if isNone(p.exitcode) <= 0 :
                p.terminate()
                p.join()
        print('START NURO 1km Mesh')

def worker_func(event_f):
    while event_f:
        job_func = jobqueue.get()

        worker_f_Processing = Process(target=job_func())
        worker_f_Processing.start()
        jobqueue.task_done()

def worker_main(event):
    while event:
        worker_m_Processing = Process(target=ccm_rec())
        worker_m_Processing.start()
        if worker_m_Processing.join(30) is None:
            worker_m_Processing.terminate()
        time.sleep(0.1)

if __name__ == '__main__':

    #set time interval
    cloud_time=10
    if config['influx_db_cloud']['interval']!='':
        cloud_time=int(config['influx_db_cloud']['interval'])
    ccm_time=1
    if config['yahoo_weather']['interval']!='':
        ccm_time=int(config['yahoo_weather']['interval'])/60.0

    #--------------<Scheduled Tasks>---------------

#    jobqueue = queue.Queue()
    jobqueue = queue.LifoQueue(maxsize=6)

    # Cloud up interval time リアルタイムでクラウド連動したい場合は、ここをコメントアウト そして、uecs_rec.py を修正
    schedule.every(cloud_time).minutes.do(jobqueue.put, job_cloud_up)

    # Yahoo_weather interval time 5
    schedule.every(5).minutes.do(jobqueue.put,job_yolp)
    schedule.every(ccm_time).minutes.do(jobqueue.put, job_yolp_ccm)

    # ifttt interval time 5
    schedule.every(5).minutes.do(jobqueue.put, job_ifttt)

    # downsampling
    schedule.every(1).hour.do(jobqueue.put, job_ds)

    # aggregate
    schedule.every(15).minutes.do(jobqueue.put, job_ag)

    # nuro1km
    schedule.every().day.at('10:13').do(jobqueue.put,job_nuro)

    #--------------<Scheduled Tasks>---------------

    #function thread
    event_f = threading.Event()
    worker_f_thread = threading.Thread(target=worker_func, args=(event_f,))
    worker_f_thread.start()
    event_f.set()

    # CCM Capture thread
    event = threading.Event()
    worker_m_thread = threading.Thread(target=worker_main, args=(event,))
    worker_m_thread.start()
    event.set()

    while True:

        if threading.active_count() < 3: # MainThread,Thread-1,Thread-2
            print('EVENT RESTART!')
#            chk_process.host_reboot()

            event_f.wait()
            event_f.clear()
            event_f.set()
            event.wait()
            event.clear()
            event.set()

        schedule.run_pending()
        time.sleep(1)

