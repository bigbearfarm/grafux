#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import subprocess
import os.path,socket
import subprocess, time
import configparser
import time,math,datetime,sys
#import logging
from datetime import datetime
import requests

#logging.basicConfig(level=logging.DEBUG)

def checkURL(url):
    ping = subprocess.Popen(["ping", "-c", "1", url],
                            stdout = subprocess.PIPE,
                            stderr = subprocess.PIPE
                            )
    out, error = ping.communicate()
    if error:
        return False
    else:
        return True

def chkInfluxDB(url):
    try:
        response = requests.get('http://'+ url +':8086/ping')
        if response.status_code == 204:
#            print("OK")
            return True
    except socket.error:
#            print(socket.error)
        pass
        print('InfluxdDB is not Running: %s' % url)
        return False

def host_reboot():
    cmd = "sudo reboot"
    subprocess.call(cmd.split())


#chkInfluxDB('localhost')
