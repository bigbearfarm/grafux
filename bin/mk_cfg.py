#-------------------------------------------------------------------------------
# Name      : mk_cfg.py
# Purpose   : grafux.cfg make
# referencd :
# Author    : ookuma yousuke
# Created   : 2018/07/30
# Copyright : (c) 2018,ookuma yousuke
# Licence   : MIT License（X11 License）
#-------------------------------------------------------------------------------
# -*- coding: utf-8 -*-
import sys,time,json,subprocess,os
from socket import *
import xml.etree.ElementTree as ET
from datetime import datetime
import configparser,codecs

def serch_table():
    print('Please wait a few minuite............')
    print('')
    print('Please wait a few minuite............')
    print('')

#    cmd = "ps -aux |grep uecs_rec|awk \'{print \"sudo kill\",$2}\' | sh"
    cmd = "ps -aux |grep uecs_main|awk \'{print \"sudo kill\",$2}\' | sh"
    subprocess.call( cmd, shell=True )  

    HOST = ''
    PORT = 16520

    s =socket(AF_INET,SOCK_DGRAM)
    s.bind((HOST, PORT))

    data=[]
    i = 0
    for i in range(0, 100):
        i+=1
        msg, address = s.recvfrom(8192)
        if msg == ".":
            print("Sender is closed")
            break
        XmlData = msg.decode('utf-8')
        root = ET.fromstring(XmlData)
        for child in root:
            #print(child.tag)
            if child.tag == "DATA" :
                c = child.get("type")
                type = str(c[0:c.find('.')])
                table = type + "_" + child.get("room") + "_" + child.get("region") + "_" + child.get("order") + "_" + child.get("priority")

        data.append(table.lower())

    data_uniq = []
    for x in data:
        i += 1
        print(" Now Loading..... " + str(i) +"   "+ x)
        if x not in data_uniq:
            data_uniq.append(x)

    return sorted(data_uniq)


def make_cfg_first(filepath):
    with open(filepath, 'w') as cfg_new:

#        cfg_new.write('\r\n[user]')
#        cfg_new.write('\r\n; https://support.google.com/accounts/answer/185833')
#        cfg_new.write('\r\ngmail=')
#        cfg_new.write('\r\npass=')
#        cfg_new.write('\r\nname=')
#        cfg_new.write('\r\nrecipient= \r\n')

        cfg_new.write('\r\n[locate]')
        cfg_new.write('\r\nlongitude = 139.648')
        cfg_new.write('\r\nlatitude = 35.858 \r\n') # saitama city

        table=[]
        table = serch_table()
        cfg_new.write('\r\n[table_name]')
        for s in table:
            cfg_new.write('\r\n'+ s + ' = ')

        cfg_new.write('\r\n')
        cfg_new.write('\r\n[influx_db]')
        cfg_new.write('\r\nlocal_host=localhost')
        cfg_new.write('\r\nuser_name=root')
        cfg_new.write('\r\nuser_pass=root')
        cfg_new.write('\r\ninterval=10')
        cfg_new.write('\r\ndb_name=uecs \r\n')

        cfg_new.write('\r\n')
        cfg_new.write('\r\n[influx_db_cloud]')
        cfg_new.write('\r\ncloud_host=')
        cfg_new.write('\r\nuser_name=root')
        cfg_new.write('\r\nuser_pass=root')
        cfg_new.write('\r\ninterval=10')
        cfg_new.write('\r\ndb_name=uecs \r\n')

#        cfg_new.write('\r\n[alertmail]')
#        cfg_new.write('\r\n')

#        cfg_new.write('\r\n[openweathermap]')
#        cfg_new.write('\r\n; https://home.openweathermap.org/users/sign_in')
#        cfg_new.write('\r\nowm_key = \r\n')

#        cfg_new.write('\r\n[twitter]')
#        cfg_new.write('\r\n; https://apps.twitter.com/')
#        cfg_new.write('\r\nCONSUMER_KEY =')
#        cfg_new.write('\r\nCONSUMER_SECRET =')
#        cfg_new.write('\r\nACCESS_KEY =')
#        cfg_new.write('\r\nCCESS_SECRET = \r\n')

        cfg_new.write('\r\n[yahoo_weather]')
        cfg_new.write('\r\nApp_ID =')
        cfg_new.write('\r\ninterval=60')
        cfg_new.write('\r\ntype = ')
        cfg_new.write('\r\nroom = ')
        cfg_new.write('\r\nregion = ')
        cfg_new.write('\r\norder = ')
        cfg_new.write('\r\npriority = ')
        cfg_new.write('\r\ndistance1 = ')
        cfg_new.write('\r\ndistance2 = ')
        cfg_new.write('\r\nsend_ccm_time = 20')
        cfg_new.write('\r\nsend_ccm_range =  \r\n')

        cfg_new.write('\r\n[ifttt]')
        cfg_new.write('\r\n#url=https://maker.ifttt.com/trigger/xxxxxxx/with/key/xxxxxxx')
        cfg_new.write('\r\nurl=')
        cfg_new.write('\r\nkey1=inairtemp_1_4_1_1,0,100')
        cfg_new.write('\r\nkey2=wairtemp_1_2_1_1,0,100 \r\n')

        cfg_new.write('\r\n[csv_file]')
        cfg_new.write('\r\nhost=localhost')
        cfg_new.write('\r\ndb_create=no')
        cfg_new.write('\r\nexport_days=300 \r\n')

# read uecs_c.cfg
filepath = os.path.dirname(os.path.abspath(__file__))
filepath = os.path.dirname(filepath)  + '/grafux.cfg'
if os.path.exists(filepath) is False:
    make_cfg_first(filepath)
    sys.exit()

config = configparser.ConfigParser()
config.read(filepath)

cfg = {}
for sect in config.sections() :
    cfg[sect] = {}
    for opt in config.options(sect) :
        cfg[sect][opt] = config[sect][opt]
 
#remove config
if os.path.exists(filepath):
    os.remove(filepath)

# make config
table=[]
table = serch_table()
table_opt =''
for s in table:
    if s in dict(cfg['table_name']) :
       table_opt = table_opt + '\r\n' + s + '= ' + cfg['table_name'][s]
    else :
       table_opt = table_opt + '\r\n' + s + '= '


with open(filepath, 'w') as cfg_new:
    for key in cfg.keys():
        cfg_new.write('\r\n[%s]' % (key))
        if key == 'table_name' :
            cfg_new.write(table_opt)
        for opt in cfg[key].keys():
            if key != 'table_name' :
                cfg_new.write('\r\n' + opt + '=' + cfg[key][opt])
        cfg_new.write('\r\n')


# print grafux.cfg
fin = codecs.open(filepath,'r','utf-8')
fout = ""
for line in fin:
    fout = fout + str(line)
print(fout)

