#-------------------------------------------------------------------------------
# Name: ifttt_webhooks.py
# Purpose: ifttt webhooks work
# referencd : https://qiita.com/undo0530/items/95dc85e3b58380ea359a
#             https://chasuke.com/python_ifttt/
# grafux.cfg : ifttt section
#           [ifttt]
#           url=https://maker.ifttt.com/trigger/{EVENT NAME}/with/key/{YOUR API KEY}
#           key1=inairtemp_1_4_1_1,8,20
#           key2=wairtemp_1_2_1_1,5,19
#
#           key +[number] =[table_name] , [row] , [high]
#
# Author   : ookuma yousuke
# Created  : 2019/01/06
# Copyright: (c) ookuma 2018
# Licence  : MIT License(X11 License)
#-------------------------------------------------------------------------------
#!/usr/bin python3
# -*- coding: utf-8 -*-
import requests
import os,configparser
import Exec_Influxdb

def Exec_ifttt(url,s1,s2,s3):
    data = {"value1": s1, "value2": s2, "value3": s3 }
    headers = {'Content-Type': "application/json"}
                                #url = 'https://maker.ifttt.com/trigger/xxxx/with/key/xxxxxxxxxxxx'
    response = requests.post(url, json=data, headers=headers)
    print( response.status_code)
    if response.status_code == 200:
        msg='ifttt is done. table:%s ,status:%s' % (s1,s2+s3)
        print(msg)

def chk_ifttt(config):
    url = config['ifttt']['url']
    list = config['ifttt'].keys()
    for s in config['ifttt'].keys():
        if s[0:3]=='key' :
            key = config['ifttt'][s]
            key_cnt = len(key.split(","))
            print("key_cnt: "+str(key_cnt))
            
            if key.split(",")[0] != '' :
                s_table = key.split(",")[0].lower()
                s_table_low = 0.0
                if key.split(",")[1] != '' :
                    s_table_low = float(key.split(",")[1])*1.0
                s_table_high = 100.0
                if key.split(",")[2] != '' :
                    s_table_high = float(key.split(",")[2])*1.0
                msg = 'Check your node!'
                if key_cnt >= 4:
                    if key.split(",")[3] != '' :
                        msg= key.split(",")[3]

                #check db
                t_list = []
                script = 'show measurements'
                r = Exec_Influxdb.select_influxdb(config,config['influx_db']['local_host'] ,s_table,None,script)
                for x in r:
                    for i in range(0,len(x)):
                        t_list.append(x[i]['name'])
#                print(t_list)
                if s_table not in t_list:
                    s1 = s_table
                    s2 = '[NO DATA]'
                    s3 = 'no table '
                    Exec_ifttt(url,s1,s2,s3)


                script =('select last(value) as last from \"%s\" where time > now() - 5m') % (s_table)
                r = Exec_Influxdb.select_influxdb(config,config['influx_db']['local_host'] ,s_table,None,script)

                if len(r)==0 :
#                    print(s_table)
                    s1 = s_table
                    s2 = '[NO DATA]'
                    s3 = 'no data within 5minutes'
                    Exec_ifttt(url,s1,s2,s3)
                else :
                    for x in r:
                        for i in range(0,len(x)):
#                        if x[0]['last'] is not None:
                            i = float(x[0]['last'])*1.0
                            print(s_table + ' : ' + str(i))
                            if s_table_low > i or s_table_high < i:
                                s1 = s_table
                                s2 = '[ ' + str(i) + ' ] Preset Value : [ ' + key.split(",")[1] + ' - ' + key.split(",")[2] + ' ]'
                                s3 = msg
                                Exec_ifttt(url,s1,s2,s3)

""" 
filepath = os.path.dirname(os.path.abspath(__file__))
filepath = os.path.dirname(filepath)  + '/grafux.cfg'
config = configparser.ConfigParser()
config.read(filepath)
chk_ifttt(config)
""" 
