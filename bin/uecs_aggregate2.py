#-------------------------------------------------------------------------------
# Name: uecs_aggregate2.py
# Purpose: 1.UECS Aggregate(ABCD average)
#            A: Night   (yesterday sunset <= Night   < today sunrise)
#            B: AM      (today sunrise    <= AM      < 12:00)
#            C: PM      (12:00            <= PM      < today sunset)
#            D: Daytime (today sunrise    <= Daytime < today sunset)
#            E: 1Day    (0:00             <= 1Day    < 23:59:59)
#           OUTPUT table --->ABC_TableName
#          
#          2.Time Aggregate()
#           OUTPUT table --->AG_Time_TableName  
#           output_value: down to the minute
#          
# referencd :
# Author:      ookuma yousuke
#
# Created: 2019/12/25
# Copyright:   (c) ookuma 2018
# Licence:     MIT License（X11 License）
#-------------------------------------------------------------------------------
#!/usr/bin python3
# -*- coding: utf-8 -*-

import os,sys,time,gc
import Initial_set,configparser
import Exec_Influxdb,CalSun
from chk_process import checkURL
from datetime import datetime
import pandas as pd
from influxdb import DataFrameClient

def aggregate(config,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC):

    json_body = []
    for table in FLG_ABC:
        # base table
#        script ='select * from \"%s\" order by time asc;' % (table)
        script ='select mean(value) from \"%s\" group by time(1d);' % (table)
        result = Exec_Influxdb.select_influxdb(config,config['influx_db']['local_host'],table,None,script)
        table_list = []
        if result is None:
            sys.exit()
        for x in result:
            for i in range(0,len(x)):
                day = x[i]['time'].split('T')[0]
                time = x[i]['time'].split('T')[1].split(':')
                table_list.append(day)

        # aggregate table
        script =('delete from \"ABC_%s\" where time >= now() - 7d; ') % (table) # delete 7days
        Exec_Influxdb.delete_influxdb(config,config['influx_db']['local_host'] ,'ABC_'+table,script)
        if config['influx_db_cloud']['cloud_host'] !='':
            Exec_Influxdb.delete_influxdb(config,config['influx_db_cloud']['cloud_host'] ,'ABC_'+table,script)

        script ='select * from \"ABC_%s\" where  time < now() - 7d order by time asc; ' % (table)
        result = Exec_Influxdb.select_influxdb(config,config['influx_db']['local_host'],table,None,script)
        ag_list = []
        for x in result:
            for i in range(0,len(x)):
                day = x[i]['time'].split('T')[0]
                time = x[i]['time'].split('T')[1].split(':')
                ag_list.append(day)

        # difference
        tb = set(table_list)
        ag = set(ag_list)
        dif = tb -ag
        print(dif)

        for d in dif:
            print(d)
            basedate1 = d.replace('-','/') + ' 12:00:00' #12h
            basedate2 = datetime.strptime(basedate1, '%Y/%m/%d %H:%M:%S') # str2datetime
            now_jst,pre_sr,pre_ss,nex_sr,nex_ss,H12,H12_utc,H00,H24 = CalSun.sun_exec(basedate2,config['locate']['longitude'],config['locate']['latitude'])

            data=[]
            print('START !! '+ table)
            # A_table Night  from pre_sunset to sunrise
            print("A_table make")
            print("Night",pre_ss,pre_sr)
            data.append(Exec_Influxdb.ABC_data(config,config['influx_db']['local_host'],table,pre_ss,pre_sr))

            # B_table AM  from  sunrise to 12:00 and same day
            print("B_table make")
            print("AM",pre_sr,H12)
            data.append(Exec_Influxdb.ABC_data(config,config['influx_db']['local_host'],table,pre_sr,H12))
            # C_table  PM from 12:00 to sunset
            print("C_table make")
            print("PM",H12,nex_ss)
            data.append(Exec_Influxdb.ABC_data(config,config['influx_db']['local_host'],table,H12,nex_ss))

            # D_table  daytime from sunrise to sunset
            print("D_table make")
            print("DAY",pre_sr,nex_ss)
            data.append(Exec_Influxdb.ABC_data(config,config['influx_db']['local_host'],table,pre_sr,nex_ss))

            # Diff  D_table-A_table
            if data[3] is not None and data[0] is not None:
                data.append(data[3]-data[0])
            else:
                data.append(None)

            # today avg
            print("today avg")
            print("TODAY",H00,H24)
            data.append(Exec_Influxdb.ABC_data(config,config['influx_db']['local_host'],table,H00,H24))


            sr = datetime.fromtimestamp(int(pre_sr)/1000000000.0) #sunrise
            ssr = str(sr.year) +'/'+ str(sr.month) +'/'+ str(sr.day) +'T'+ str(sr.hour) +':' + str(sr.minute)
            ss = datetime.fromtimestamp(int(nex_ss)/1000000000.0) #sunset
            sss = str(ss.year) +'/'+ str(ss.month) +'/'+ str(ss.day) +'T'+ str(ss.hour) +':' + str(ss.minute)

            json_body.append({
                                      "measurement": 'ABC_'+table,
                                      "time": H12_utc, #time,
                                      "precision": "m",
                                      "fields": {
                                          "Sunrise":ssr,
                                          "Sunset":sss,
                                          "Night": data[0] ,
                                          "AM": data[1] ,
                                          "PM": data[2] ,
                                          "Day time":data[3] ,
                                          "Day-Night":data[4],
                                          "TodayAVG":data[5]
                                          }
                              })

            print("Insert ABC_"+ table + "("+ str(H12_utc) +")")

        del tb,ag,dif
        # メモリ解放
        gc.collect()

    Exec_Influxdb.aggregate_influxdb(config,config['influx_db']['local_host'] , json_body)
    if config['influx_db_cloud']['cloud_host'] !='':
        Exec_Influxdb.aggregate_influxdb(config,config['influx_db_cloud']['cloud_host'] , json_body)
        print("Cloud up -------------------!")


def time_ag(config,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC):
    cloud_time=10
    if config['influx_db_cloud']['interval']!='':
        cloud_time=int(config['influx_db_cloud']['interval'])

    if config.has_section('time_aggregate') is False:
        print("no section")
        sys.exit()
        
    for key in config['time_aggregate'].keys():
        table = key
        low_val = float(config['time_aggregate'][key].split(',')[0])*1.0
        high_val = float(config['time_aggregate'][key].split(',')[1])*1.0

        # time aggregate table delete
        script =('delete from \"AG_Time_%s\" where time >= now() - 2d; ') % (table) # delete 2days
        Exec_Influxdb.delete_influxdb(config,config['influx_db']['local_host'] ,'AG_Time_'+table,script)
        if config['influx_db_cloud']['cloud_host'] !='':
            Exec_Influxdb.delete_influxdb(config,config['influx_db_cloud']['cloud_host'] ,'AG_Time_'+table,script)

        # row data
        script ='select mean(value) from \"%s\" group by time(1d);' % (table)
        result = Exec_Influxdb.select_influxdb(config,config['influx_db']['local_host'],table,None,script)
        table_list = []
        for x in result:
            for i in range(0,len(x)):
                day = x[i]['time'].split('T')[0]
                time = x[i]['time'].split('T')[1].split(':')
                table_list.append(day)

        # aggregate data
        script ='select mean(value) from \"AG_Time_%s\" group by time(1d); ' % (table)
        result = Exec_Influxdb.select_influxdb(config,config['influx_db']['local_host'],table,None,script)
        ag_list = []
        for x in result:
            for i in range(0,len(x)):
                if x[i]['mean'] is not None:  #20221120
                    day = x[i]['time'].split('T')[0]
                    time = x[i]['time'].split('T')[1].split(':')
                    ag_list.append(day)

        # difference
        tb = set(table_list)
        ag = set(ag_list)
        dif = tb -ag

        for d in dif:
            print(d)
            basedate1 = d.replace('-','/') + ' 12:00:00' #12h
            basedate2 = datetime.strptime(basedate1, '%Y/%m/%d %H:%M:%S') # str2datetime
            now_jst,pre_sr,pre_ss,nex_sr,nex_ss,H12,H12_utc,H00,H24 = CalSun.sun_exec(basedate2,config['locate']['longitude'],config['locate']['latitude'])

            script ='select mean(\"value\") as value from \"%s\" where time > %s and time < %s group by time(%sm) ;' % (table,H00,H24,cloud_time)
#        print(script)
            db_user,db_pass,db_database = Exec_Influxdb.set_dbuser(config,config['influx_db']['local_host'])
            client = DataFrameClient(config['influx_db']['local_host'], 8086, db_user, db_pass, db_database, timeout=3, retries=6 )
            try:
                r = client.query(script)
            except :
                continue

            print(table)
            if len(r)<=0:
                continue

            df = pd.DataFrame(r[table])
            df = df.dropna(axis = 0, how = 'any')
            cnt=0
            if len(df)>0:
                for index, row in df.iterrows():
#                print(row['value'])
                    if row['value'] >= low_val and row['value'] <= high_val:
                        cnt = cnt + 1
                print('CNT: '+str(cnt))
            # time convert
            H001 = datetime.fromtimestamp(int(H00)/1000000000.0) # 00 
            H001 = str(H001.year) +'/'+ str(H001.month) +'/'+ str(H001.day) +'T'+ str(H001.hour) +':' + str(H001.minute)
            sum_time = cnt * cloud_time *1.0
            
            Exec_Influxdb.insert_influxdb(config,config['influx_db']['local_host'],'AG_Time_'+table ,sum_time ,None,'1',H001)
            #Cloud influxDB
            if config['influx_db_cloud']['cloud_host'] != '' :
                Exec_Influxdb.insert_influxdb(config,config['influx_db_cloud']['cloud_host'],'AG_Time_'+table ,sum_time ,None,'1',H001)

            del df
            # メモリ解放
            gc.collect()

"""
filepath = os.path.dirname(os.path.abspath(__file__))
filepath = os.path.dirname(filepath)  + '/grafux.cfg'
config = configparser.ConfigParser()
config.read(filepath)

# FLG up
FLG_UP=[]
FLG_DIFF=[]
FLG_MAX=[]
FLG_ABC=[]
FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC = Initial_set.chk_table(config)

aggregate(config,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC)
time_ag(config,FLG_UP,FLG_DIFF,FLG_MAX,FLG_ABC)

"""