#-------------------------------------------------------------------------------
# Name      : csv_influxdb.py
# Purpose   : UECS CSV import & export
# referencd : https://github.com/fabio-miranda/csv-to-influxdb
# Author    : ookuma yousuke
# Created   : 2019/08/25
# Copyright : (c) 2017, Fabio Miranda (c) 2018,ookuma yousuke
# Licence   : BSD 3-Clause License
#-------------------------------------------------------------------------------
#!/usr/bin python3
# -*- coding: utf-8 -*-
import requests,json,gzip,gc
import argparse
import csv,datetime
from influxdb import InfluxDBClient
import configparser,os,sys
import Exec_Influxdb
import pandas as pd
from os.path import join, getsize
import zipfile
from datetime import datetime, timedelta, timezone,date

#
# CSV EXPORT
#
def exportCSV(config,host,None1,None2,None3,None4):
    csvpath = os.path.dirname(os.path.abspath(__file__))
    csvpath = os.path.dirname(csvpath)+ '/data'

    for root, dirs, files in os.walk(csvpath):
#        print(root, "consumes", end=" ")
#        print(sum(getsize(join(root, name)) for name in files), end=" ")
#        print("bytes in", len(files), "non-directory files")
        print(files)
        for f in files:
            name, ext = os.path.splitext(f)
#            print(name,ext)
            if ext in ('.csv','.zip') :
                print(csvpath +'/'+ f)
                os.remove(csvpath +'/'+ f)


    script = 'SHOW MEASUREMENTS'
    # table & func is None
    r = Exec_Influxdb.select_influxdb(config,host,None,None,script)
    table_list=[]
    for x in r:
        for i in range(0,len(x)):
            table_list.append(x[i]['name'])

    cnt_all=len(table_list)
    cnt =0
    for table in table_list:
        cnt = 1 + cnt
        if config['csv_file']['export_days']=='':
            script_B = ' where time > now() -32d order by time asc '
        else :
            script_B = ' where time > now() -%sd order by time asc ' % (config['csv_file']['export_days'])
        
        script_A='select * from \"' + table + '\"' + script_B 
#        script_A='select * from \"' + table + '\" order by time '
        r1 = Exec_Influxdb.select_influxdb(config,host,table,None,script_A)
        print(script_A)
        try:
            r2 = list(r1.get_points(measurement=table))
        except:
            continue

        df = pd.DataFrame(r2)
#        print('count: '+ str(df.shape[0]))
        if df.shape[0] > 1 :
            print('['+ str(cnt) +'/'+ str(cnt_all) + '][EXPORT FILE] : ' + os.path.dirname(csvpath) + '/data/'+ table+'.csv')
            print('[table_name] : ' + table)
            df.to_csv(os.path.dirname(csvpath) + '/data/'+ table+'.csv', index=None)
            print('Output %d lines done' % df.shape[0])
        del r1,r2,df
        gc.collect()
        print(" ")

##
## CSV IMPORT
##
epoch = datetime.utcfromtimestamp(0)
def unix_time_millis(dt):
    return int((dt - epoch).total_seconds() * 1000)

## Check if data type of field is float
def isfloat(value):
        try:
            float(value)
            return True
        except:
            return False

## Check if data type of field is int
def isinteger(value):
        try:
            if(float(value).is_integer()):
                return True
            else:
                return False
        except:
            return False

# Check if date
def validate_date(set_datetime,timeformat):
        try:
            unix_time_millis(datetime.strptime(set_datetime,timeformat))
            return True
        except :
            return False

def loadCsv(inputfilename, servername, user, password, dbname, metric, timecolumn, timeformat, tagcolumns, fieldcolumns, usegzip, delimiter, batchsize,time_format,restore):

    host = servername #servername[0:servername.rfind(':')]
    port = 8086 #int(servername[servername.rfind(':')+1:])
    client = InfluxDBClient(host, port, user, password, dbname)

    # format tags and fields
    if tagcolumns:
        tagcolumns = tagcolumns.split(',')
    if fieldcolumns:
        fieldcolumns = fieldcolumns.split(',')

    # open csv
    datapoints = []
    inputfile = open(inputfilename, 'r')
    count = 0
    with open(inputfilename, 'r') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=delimiter)
        for row in reader:
            timestamp = row[timecolumn] # unix_time_millis(datetime.datetime.strptime(row[timecolumn],timeformat)) * 1000000 # in nanoseconds
            if restore=='1' : #1: other_CSV ->Influxdb
                year = row[timecolumn].split('/')[0]
                timestamp = unix_time_millis(datetime.strptime(row[timecolumn],timeformat)) * 1000000
#                print(timestamp)
#                print(timestamp,datetime.strptime(row[timecolumn],timeformat),row[timecolumn])
#                if validate_date(set_datetime,timeformat):
#                    timestamp = unix_time_millis(datetime.strptime(set_datetime,timeformat)) * 1000000 # in nanoseconds
#                else:
#                   print("skip data: " + timestamp,datetime.strptime(row[timecolumn],timeformat))
#                   next(reader) 
#                                                                                 1532995200000000000
            tags = {}
#            print(timestamp,datetime.strptime(row[timecolumn],timeformat))

            for t in tagcolumns:
                v = 0
                if t in row:
                    v = row[t]
                tags[t] = v
#            print(str(tags))

            if restore=='1' : #1: other_CSV ->Influxdb
                tags['year'] = year

            fields = {}
            for f in fieldcolumns:
                v = 0
                if f in row:
                    v = float(row[f]) if isfloat(row[f]) else row[f]
                fields[f] = v


            point = {"measurement": metric, "time": timestamp, "fields": fields, "tags": tags}

            datapoints.append(point)
            count+=1
            
            if len(datapoints) % batchsize == 0:
                print('Read %d lines'%count)
                print('Inserting %d datapoints...'%(len(datapoints)))
                response = client.write_points(datapoints)

                if response == False:
                    print('Problem inserting points, exiting...')
                    exit(1)

                print("Wrote %d, response: %s" % (len(datapoints), response))


                datapoints = []
            

    # write rest
    if len(datapoints) > 0:
        print('Read %d lines'%count)
        print('Inserting %d datapoints...'%(len(datapoints)))
        response = client.write_points(datapoints)

        if response == False:
            print('Problem inserting points, exiting...')
            exit(1)

        print("Wrote %d, response: %s" % (len(datapoints), response))

    print('Done')
    

def importCSV(config,host,drop_table,restore,time_format,tag):

    csv_gzip=False
    csv_delimiter=','

    if  host == 'localhost':
        csv_server = config['influx_db']['local_host'] 
        csv_user = config['influx_db']['user_name']
        csv_password = config['influx_db']['user_pass']
        csv_dbname = config['influx_db']['db_name']
    elif host == 'cloud_host':
        csv_server = config['influx_db']['cloud_host'] 
        csv_user = config['influx_db_cloud']['user_name']
        csv_password = config['influx_db_cloud']['user_pass']
        csv_dbname = config['influx_db_cloud']['db_name']
    else:
        sys.exit

    #check databases
    if config['csv_file']['db_create']=='yes':
        print(' DROP DATABASE : ' + csv_dbname)
        script = 'DROP DATABASE ' + csv_dbname
        Exec_Influxdb.select_influxdb(config,csv_server,None,None,script) # table & func is None

    script = 'SHOW DATABASES'
    r = Exec_Influxdb.select_influxdb(config,csv_server,None,None,script) # table & func is None
    db_list=[]
    for x in r:
        for i in range(0,len(x)):
            db_list.append(x[i]['name'])
    if csv_dbname not in db_list:
        print(' CREATE DATABASE : ' + csv_dbname)
        script = 'CREATE DATABASE ' + csv_dbname
        Exec_Influxdb.select_influxdb(config,csv_server,None,None,script)

    tagcolumns = ['sum','direction']
    if tag =='' or tag is not None:
        tagcolumns = tag
#    fieldcolumns=['value','fc_time','AM','Day time','Day-Night','Night','PM','Sunrise','Sunset','TodayAVG',
#                  'fc10','fc15','fc20','fc25','fc30','fc35','fc40','fc45','fc5','fc50','geohash','lat','lon','ob']

    #check file
    csvpath = os.path.dirname(filepath)  + '/data'
    for root, dirs, files in os.walk(csvpath):
#        print(files)
        cnt_all = len(files)
        cnt=0
        for f in files:
            cnt= 1 + cnt
            name, ext = os.path.splitext(f)
#            print(name,ext)
            if ext == '.csv' :

                if drop_table == '1':
                    print(' DROP MEASUREMENT : ' + name)
                    script = 'DROP MEASUREMENT ' + name
                    Exec_Influxdb.select_influxdb(config,csv_server,None,None,script)
                    print("drop")

#                print(csvpath +'/'+ f)
                csv_input=csvpath +'/'+ f
                csv_metricname = name
                csv_timecolumn = 'time'
                csv_timeformat = '%Y-%m-%d %H:%M:%S'
                if time_format!='' or time_format is not None:
                    csv_timeformat = time_format
                with open(csv_input, 'r') as f2:
                    reader = csv.reader(f2) # reader objects
                    header = next(reader)  # header 
                    csv_batchsize=sum(1 for line in reader) #line count
                    csv_tagcolumns=''
                    csv_fieldcolumns=''
                    for s in header:
                        if s in tagcolumns:
                            csv_tagcolumns = csv_tagcolumns +','+ s
                        elif s not in tagcolumns:
                            csv_fieldcolumns = csv_fieldcolumns +','+ s

                    print('[' + str(cnt) + '/'+ str(cnt_all) + '][IMPORT_CSV] : ' + name )
                    print(csvpath +'/'+ f)
                    loadCsv(csv_input, csv_server, csv_user, csv_password, csv_dbname,
                            csv_metricname, csv_timecolumn, csv_timeformat, csv_tagcolumns,
                            csv_fieldcolumns, csv_gzip, csv_delimiter, csv_batchsize,time_format,restore)


if __name__ == "__main__":

    filepath = os.path.dirname(os.path.abspath(__file__))
    filepath = os.path.dirname(filepath)  + '/grafux.cfg'
    config = configparser.ConfigParser()
    config.read(filepath)

    if config['csv_file']['host']!='':
        argv = sys.argv

        if argv[1]=='import':
            if argv[3]=='0': #0: Influxdb_CSV ->Influxdb(defalut), 1: other_CSV ->Influxdb
                importCSV(config,config['csv_file']['host'],None,None,None,None)
            else:
                tag=[]
                time_format=argv[4] #
                for i in range(0,len(argv[5].split(','))): # tmp_tag1,tmp_tag2,r_tag1,r_tag2
                    tag.append(argv[5].split(',')[i])
                if tag not in ['year']:
                    tag.append('year')
                print(tag)
                print(time_format)
                importCSV(config,config['csv_file']['host'],argv[2],argv[3],time_format,tag)
        else:
            exportCSV(config,config['csv_file']['host'],None,None,None,None)
    else :
        print('CHECK : '+ filepath)


