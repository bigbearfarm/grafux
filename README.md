# **Grafux on Raspberrypi**

* Grafuxは、日本の施設園芸農業のUECS通信データをキャプチャして、[InfluxDB](https://docs.influxdata.com/influxdb/v1.6/)に格納します。
* 格納されたデータを、[Grafana](https://play.grafana.org/d/000000012/grafana-play-home?orgId=1)で描画することにより、データを照会し、視覚化し、警告し、理解することができます。チームとダッシュボードを作成、探索、共有し、データドリブンなDIY農業文化を育成することの一助になります。
* [UECS Protcol is Japanese Green House UDP broadcast Protcol](https://uecs.jp/)
* Grafana × UECS で Grafuxという名称にしています。
* [WIKI](https://bitbucket.org/bigbearfarm/grafux/wiki/Home)に動画も載せておきました。
* [YOLP Weather](https://developer.yahoo.co.jp/webapi/map/openlocalplatform/v1/weather.html) の降雨情報 UECS通信CCM化。
* [IFTTTのWebhooks](https://ifttt-japan.club/knowledge/this/webhooks)対応。
* influxdbデータベースのダウンサンプリング。
* CSVデータの出入力機能。  
* [Influxdb-timeshift-proxy](https://github.com/maxsivanov/influxdb-timeshift-proxy)対応によるタイムシフト機能・クエリ間（つまりメトリック間）の数学的演算を実行。  
* [農研機構 メッシュ農業気象データシステム](https://amu.rd.naro.go.jp/)対応 ※[ライブラリ等を別途、設定する必要あり。](https://bitbucket.org/bigbearfarm/grafux/wiki/%E3%83%A1%E3%83%83%E3%82%B7%E3%83%A5%E8%BE%B2%E6%A5%AD%E6%B0%97%E8%B1%A1%E3%83%87%E3%83%BC%E3%82%BF)  

![サンプル][a]
[a]:https://blog-imgs-119.fc2.com/b/i/g/bigbearfarm/Grafux_setsumeis.png  
---
## 動作環境
* Raspberrypi3 [Raspbian Buster Lite(Release :2019-09-26)](https://www.raspberrypi.org/downloads/raspbian/)
* MicroSD Card 16G or more / class10 / MLC

## 事前準備
* SSHクライアントやFTPクライアント、LINUXコマンドが使えることが前提条件になります。
* SSHの許可や言語の設定等を行っておいてください。  
`$ sudo raspi-config` 
* [固定IPアドレスを設定してください。](https://qiita.com/BearcubThaw/items/cde2494556d609115fb6)
---
## Version
* [InfluxDB 1.7.9](https://docs.influxdata.com/influxdb/v1.7/)
* [Grafana-6.4.3](https://grafana.com/grafana/download) ※Ubuntu & Debian(ARMv7)と(ARM64)があり2018.06よりRaspberrypiに公式対応しています。
--- 
## ソフトウェア Install
1. パッケージリストの更新  
`$ sudo apt-get update -y`
2. git インストール  
`$ sudo apt-get install git -y`
3. pip3 インストール  
`$ sudo apt-get install python3-pip -y`
4. adduserとlibfontconfig インストール  
`$ sudo apt-get install -y adduser libfontconfig`
---
## Python3 Library Install
1. `$ sudo pip3 install requests`  
2. `$ sudo apt-get install python3-influxdb`  
3. `$ sudo apt-get install python3-pandas -y`  
4. `$ sudo pip3 install docutils`
5. `$ sudo pip3 install python-geohash`
6. `$ sudo pip3 install pyephem` 
7. `$ sudo pip3 install schedule` 
8. `$ sudo apt-get install expect -y` 
---
# Influxdb Install & Setup
1. ダウンロード  
`$ wget https://dl.influxdata.com/influxdb/releases/influxdb-1.7.9_linux_armhf.tar.gz`  

2. user 追加  
`$ sudo adduser influxdb`  
パスワードに`root`を入れるが、他は空欄で`ENTER`  
Enter new UNIX password:`root`  (←入力しても表示されない。rootといれたら、次はEnterを押下してください)  
Retype new UNIX password:`root`  (←入力しても表示されない。rootといれたら、次はEnterを押下してください)  
Enter the new value, or press ENTER for the default  
Full Name []: ` `  
Room Number []:` `  
Work Phone []:` `  
Home Phone []:` `  
Other []:` `  
Is the information correct? [Y/n] `y`  
`$ id influxdb`  
`uid=1001(influxdb) gid=1001(influxdb) groups=1001(influxdb)` 表示されていることを確認

3. 解凍＆解凍したファイルをディレクトリ直下にコピー  
`$ sudo tar xvfz influxdb-1.7.9_linux_armhf.tar.gz`  
`$ ls` で解凍したディレクトリを探す＆そのディレクトリに移動  
`$ cd influxdb-1.7.9-1`  
`$ sudo cp -R * /`  
`$ cd` 
4. コピーしたInfluxdbファイルに”influxdb”ユーザーの所有権限を付与  
`$ sudo chown influxdb:influxdb -R /etc/influxdb`  
`$ sudo chown influxdb:influxdb -R /var/log/influxdb`  
`$ sudo mkdir -p /var/lib/influxdb`  
`$ sudo chown influxdb:influxdb -R /var/lib/influxdb`
5. 自動起動のスクリプトをinitdに設置＆権限付与  
`$ sudo cp /usr/lib/influxdb/scripts/init.sh /etc/init.d/influxdb`  
`$ sudo chmod 755 /etc/init.d/influxdb`  
`$ sudo cp /usr/lib/influxdb/scripts/influxdb.service /etc/systemd/system`
6. 再起動  
`$ sudo reboot`
7. Influxdb起動  
`$ sudo /etc/init.d/influxdb start`
8. その他　自動起動設定  
`$ sudo systemctl start influxdb.service`  
`$ sudo systemctl enable influxdb.service` 
---
# Influxdb-timeshift-proxy Install & Setup(任意)
[Influxdb-timeshift-proxyとは](https://github.com/maxsivanov/influxdb-timeshift-proxy/)  
このプロキシサーバーはinfluxDBサーバーにタイムシフト機能を追加します。この機能はグラファナの期間を比較するのに非常に役立ちます。  
プロキシは、クエリ間（つまりメトリック間）の数学的演算を実行できます。  

1. nodejsインストール  
`$ cd /opt`  
`$ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -`  
`$ sudo apt-get install -y nodejs`  

2. Influxdb-timeshift-proxyを取得  
`$ sudo git clone https://github.com/maxsivanov/influxdb-timeshift-proxy.git`  
`$ cd influxdb-timeshift-proxy`  

3. npmインストール  
`$ sudo npm i `  
---
# Grafana Install
1. どのバージョンをダウンロードするか確認  
`$ uname -a`  
Linux Grafana 4.9.59-v7+ #1047 SMP Sun Oct 29 12:19:23 GMT 2017 armv7l GNU/Linux  
**v7**とあるので、**Ubuntu & Debian(ARMv7)**をダウンロードします  
そうでない場合、[こちらの公式ページ](https://grafana.com/grafana/download?platform=arm )を確認してパッケージをダウンロードしてください
2. **v7**の場合のダウンロード＆解凍  
`$ cd`  
`$ wget https://dl.grafana.com/oss/release/grafana_6.4.3_armhf.deb `  

2. パッケージにgrafana_6.4.3_armhf.debを追加  
`$ sudo dpkg -i grafana_6.4.3_armhf.deb` 

3. 自動起動設定  
`$ sudo service grafana-server start`  
`$ sudo /bin/systemctl start grafana-server`  
`$ sudo /bin/systemctl enable grafana-server`

4. パネルの追加  
`$ sudo grafana-cli plugins install grafana-clock-panel`  
`$ sudo grafana-cli plugins install grafana-worldmap-panel`  
`$ sudo grafana-cli plugins install natel-influx-admin-panel`  
`$ sudo grafana-cli plugins install ryantxu-annolist-panel`  

5. リダイレクト設定(任意)  
`$ sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 3000`

---
# その他
1. スワップファイル拡張
100Mだったものを1Gにします。  
`$ free -m`

```
              total        used        free      shared  buff/cache   available
Mem:         949452       63640      689396        6316      196416      827188
Swap:        102396           0      102396
```

`$ sudo nano /etc/dphys-swapfile`  

```
#CONF_SWAPSIZE=100
CONF_SWAPSIZE=1028
```

`$ sudo dphys-swapfile install`  
`$ sudo dphys-swapfile swapon`  

---

# Grafux Install
1. Grafux インストール  
`$ cd /opt`  
`$ sudo git clone https://bigbearfarm@bitbucket.org/bigbearfarm/grafux.git`
2. 所有権変更＆実行権限付与  
`$ sudo chown pi:pi -R /opt/grafux`  
`$ sudo chmod +x -R grafux`  
3. データベース作成 Influxdbに「uecs」データベースを作成します。  
`$ cd /opt/grafux/cli `  
`$ ./influx_create_db.sh`  
4. grafux.cfg の作成  
`$ cd /opt/grafux/bin`  
`$ sudo python3 mk_cfg.py` 100秒かかります。その間に受信したCCMを設定ファイルに書き込みます。  
`$ sudo nano /opt/grafux/grafux.cfg `  
grafux.cfgが作成されているこを確認し、編集ください。  

## Grafux 設定ファイル編集(grafux.cfg) 
1. locate セクション  
2. table_name セクション  
は、必ず設定してください。他は設定しなくても動作するはずです。  
https://bitbucket.org/bigbearfarm/grafux/wiki/Home

## Grafux 自動起動設定  
1. Grafux 自動起動  
`$ cd /opt/grafux/cli`  
`$ sudo cp grafux.service /etc/systemd/system`  
`$ sudo systemctl start grafux.service`  
`$ sudo systemctl enable grafux.service`  
2. influxdb-timeshift-proxy 自動起動(任意)  
`$ sudo cp influxdb-timeshift-proxy.service /etc/systemd/system`  
`$ sudo systemctl start influxdb-timeshift-proxy.service`  
`$ sudo systemctl enable influxdb-timeshift-proxy.service`  
3. 再起動で動作開始  
 `$ sudo reboot`  

で実行済です。5分後からデータ取得されます
ブラウザでURL欄にラズベリーパイのIPアドレスを打ち込んで下さい。  
「192.168.xx.xx」  
Grafanaが起動します。起動しない場合、「192.168.xx.xx:3000」  
---
# Grafana 設定
USERID：admin  
PASSWORD：admin  

## データソース(datasources)の設定をします。  

## ダッシュボードのサンプルをインポートします。  
/opt/grafux/sample に入っているファイルを操作している端末に移動してください。  
Grafanaにログインした後、Grafanaの左「＋」よりimportを選択しsampleフォルダにある「×××××.json」を選択してuploadしてください。  
