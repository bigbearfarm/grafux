#!/bin/sh
echo "---------------------------------------------------"
echo " ...UECS InfluxDB CSV IMPORT ...                   "
echo " param                                             "
echo " 1: import or nothing                              "
echo " 2:table drop                                      "
echo "       0:no (defalut)                              "
echo "       1: yes                                      "
echo " 3:restore                                         "
echo "       0: Influxdb_CSV ->Influxdb(defalut)         "
echo "       1: other_CSV ->Influxdb                     "
echo " 4: datetime YYYYMMDD                              "
echo "    e.m                                            "
echo "        2017-12-19T13:50:00Z  -> %Y-%m-%dT%H:%M:%S "
echo "        2018/2/1              -> %Y/%m/%d          "
echo " 5:tag coloums                                     "
echo "    e.m                                            "
echo "        tmp_tag1,tmp_tag2,rain,r_tag1,r_tag2       "
echo "---------------------------------------------------"
s=" "
#defalut restore Influxdb_CSV ->Influxdb(defalut)
prm1="import"
prm2="1"
prm3="0"
prm4="%Y-%m-%dT%H:%M:%S"
prm5="sum,direction"

#Ninni CSV restore
#prm1="import"
#prm2="1"
#prm3="1"
#prm4="%Y/%m/%d"
#prm5="tmp_tag1,tmp_tag2,r_tag1,r_tag2"

echo "PRAM1: "$prm1
echo "PRAM2: "$prm2
echo "PRAM3: "$prm3
echo "PRAM4: "$prm4
echo "PRAM5: "$prm5

cd /opt/grafux
DIR_NAME=`pwd`
script=$DIR_NAME"/bin/csv_influxdb.py "$prm1$s$prm2$s$prm3$s$prm4$s$prm5

echo "sudo python3 "$script
sudo python3 $script


