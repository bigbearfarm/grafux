#!/bin/bash

echo "----------------------------------"
echo " ...Grafux Setup ..."
echo "----------------------------------"
cd /tmp

echo "----------------------------------"
echo " ... 1:Soft install..."
echo "----------------------------------"
sudo apt-get update -y
sudo apt-get install git -y
sudo apt-get install python3-pip -y
sudo apt-get install -y adduser libfontconfig

echo "----------------------------------"
echo "... 2:Python3 Library Install..."
echo "----------------------------------"
sudo pip3 install requests
sudo apt-get install python3-pandas -y
sudo pip3 install docutils
sudo pip3 install python-geohash
sudo pip3 install pyephem
sudo apt-get install expect -y
echo
sudo git clone git://github.com/influxdata/influxdb-python.git
cd influxdb-python
sudo python3 setup.py install
echo
echo "----------------------------------"
echo "... 3:Influxdb Install & Setup ..."
echo "----------------------------------"
cd /tmp
wget https://dl.influxdata.com/influxdb/releases/influxdb-1.6.0_linux_armhf.tar.gz
sleep 5s
CNT=$(cut -d: -f1 /etc/passwd | grep -c "influxdb")
if [ $CNT -eq 0 ]
then
echo "... user add ..."
expect -c "
    set timeout 3
    spawn sudo adduser influxdb
    expect \"Enter new UNIX password:\"
    send -- \"root\n\"
    expect \"Retype new UNIX password:\"
    send -- \"root\n\"
    expect \"Full Name []:\"
    send -- \"\n\"
    expect \"Room Number []:\"
    send -- \"\n\"
    expect \"Work Phone []:\"
    send -- \"\n\"
    expect \"Home Phone []:\"
    send -- \"\n\"
    expect \"Other []:\"
    send -- \"\n\"
    expect \"Is the information correct? \[Y/n\]\"
    send -- \"Y\n\"
"
else
   echo "skip adduser"
fi

echo
sleep 5s

sudo tar xvfz influxdb-1.6.0_linux_armhf.tar.gz
cd influxdb-1.6.0-1
sudo cp -R * /
sudo chown influxdb:influxdb -R /etc/influxdb
sudo chown influxdb:influxdb -R /var/log/influxdb
sudo mkdir -p /var/lib/influxdb
sudo chown influxdb:influxdb -R /var/lib/influxdb
sudo cp /usr/lib/influxdb/scripts/init.sh /etc/init.d/influxdb
sudo chmod 755 /etc/init.d/influxdb
sudo cp /usr/lib/influxdb/scripts/influxdb.service /etc/systemd/system
sudo /etc/init.d/influxdb start
sudo systemctl start influxdb.service
sudo systemctl enable influxdb.service

cd 
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
git clone https://github.com/maxsivanov/influxdb-timeshift-proxy.git
cd influxdb-timeshift-proxy
sudo npm i 


echo "----------------------------------"
echo "... 4:Grafana Install ..."
echo "----------------------------------"
cd /tmp
CNT=$(dpkg -l | grep -c "grafana")
if [ $CNT -eq 0 ]
then
    echo "Grafana Install"
    wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_5.2.1_armhf.deb
    sleep 5s
    sudo dpkg -i grafana_5.2.1_armhf.deb
    sudo service grafana-server start
    sudo /bin/systemctl start grafana-server
    sudo /bin/systemctl enable grafana-server

    sudo grafana-cli plugins install grafana-clock-panel
    sudo grafana-cli plugins install grafana-worldmap-panel
    sudo grafana-cli plugins install natel-influx-admin-panel
else
    echo "skip Grafana"
fi

echo "----------------------------------"
echo "... 5:Swapfile edit ..."
echo "----------------------------------"
sudo sed -i s/CONF_SWAPSIZE=100/CONF_SWAPSIZE=1028/ /etc/dphys-swapfile
sudo dphys-swapfile install
sudo dphys-swapfile swapon

echo "----------------------------------"
echo "... 6:Grafux Install ..."
echo "----------------------------------"
cd /home/pi/grafux/cli
./influx_create_db.sh

cd /home/pi/grafux/bin
sudo python3 mk_cfg.py


