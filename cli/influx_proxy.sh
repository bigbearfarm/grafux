#!/bin/sh
echo "----------------------------------"
echo "influxdb-timeshift-proxy start ......"
echo "----------------------------------"
cd /opt
#cd /home/pi

#CNT=$(ps aux | grep -c "node ./bin/www")
CNT=$(netstat -l | grep -c "8089 .* LISTEN")
echo $CNT

echo date '+%Y/%m/%d%T'
# $CNT = 0
if [ $CNT -le 1 ]  # $CNT < 1
then
  echo "influxdb-timeshift-proxy start ......"
  cd /opt/influxdb-timeshift-proxy
#  cd /home/pi/influxdb-timeshift-proxy
  sudo INFLUXDB=localhost:8086 npm run start
  
fi


